package com.demo.pgtest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.pgtest.model.Student;
import com.demo.pgtest.repository.StudentRepository;

@RestController
@RequestMapping("/api/auth")
public class StudentController {
	@Autowired
	private StudentRepository studentRepository;

	@CrossOrigin
	@PostMapping("/student")
	public Student createStudent(@Valid @RequestBody Student student) {
		Student student1 = studentRepository.save(student);
		return student1;
	}

	@CrossOrigin
	@GetMapping("/student")
	public Page<Student> listStudent(Pageable pageable) {
		return studentRepository.findAll(pageable);
	}

	@CrossOrigin
	@GetMapping("/student/{id}")
	public Student getById(@PathVariable Long id) {
		Student student = studentRepository.getOne(id);
		return student;
	}
	
//	@CrossOrigin
//	@DeleteMapping("/student")
//	public ResponseEntity<?> deleteStudent(@PathVariable Long id){
//		return studentRepository.findById(id).
//				map(( student -> {
//					studentRepository.delete(student);
//					return ResponseEntity.ok().build();
//					
//				}).
//	}
}
