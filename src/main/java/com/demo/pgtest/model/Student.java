package com.demo.pgtest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "student")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Student {
	
	@Id
	@Column(name = "id", nullable = false)
	@SequenceGenerator(name = "student_seq", sequenceName = "student_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="student_seq")
	private Long id;
	private String firstname;
	private String lastname;
}
