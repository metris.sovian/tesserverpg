package com.demo.pgtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.pgtest.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
	
}
